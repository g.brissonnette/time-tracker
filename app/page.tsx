import { getServerSession } from "next-auth";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";

import Login from "@/components/login";
import TimerHistory from "@/components/history/timerHistory";
import UserInfo from "@/components/profile/userInfo";
import ControlePane from "@/components/timer/controlePane";
import Header from "@/components/header";

const Tracking = () => {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between w-screen">
      <Header />
      <div className="flex flex-col items-center w-screen">
        <div className="mb-24">
          <UserInfo />
        </div>
        <div className="mb-24">
          <ControlePane />
        </div>
        <div className="w-5/6 lg:w-2/3">
          <TimerHistory />
        </div>
      </div>
    </main>
  )
}

export default async function page() {
  const session = await getServerSession(authOptions);
  return (
    <>
    {session ? <Tracking /> : <Login />}
    </>
  );
}
