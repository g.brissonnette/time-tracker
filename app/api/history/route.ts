import { NextResponse, NextRequest } from "next/server";
import { User, TimerHistoryAPI } from "@/types/user";
import { executeIfAuthenticatedReturn401IfNot } from "@/technical/api/security";
import { getUserData } from "@/repository/user";
import { replaceExistingUserHistory } from "@/repository/history";

export async function GET(request: NextRequest) {
  return executeIfAuthenticatedReturn401IfNot(
    request,
    async (_request, token) => {
      const user: User = await getUserData(token.uid);
      return NextResponse.json({ history: user.history }, { status: 200 });
    }
  );
}

export async function POST(request: NextRequest) {
  return executeIfAuthenticatedReturn401IfNot(
    request,
    async (_request, token) => {
      const newHistory: TimerHistoryAPI = await request.json();
      await replaceExistingUserHistory(token.uid, newHistory);
      return NextResponse.json({ status: 201 });
    }
  );
}
