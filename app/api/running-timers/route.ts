import {
  resetExistingTimer,
  updateExistingTimer,
} from "@/repository/runningTimer";
import { getUserData } from "@/repository/user";
import { executeIfAuthenticatedReturn401IfNot } from "@/technical/api/security";
import { RunningTimer, RunningTimersAPI } from "@/types/runningTimer";
import { User } from "@/types/user";
import { NextRequest, NextResponse } from "next/server";

const mapResponse = (user: User): RunningTimersAPI => {
  if (user.currentRunningTimer.startDate) {
    return {
      runningTimers: [
        {
          startDate: user.currentRunningTimer.startDate,
          description: user.currentRunningTimer.description,
          increasing: user.currentRunningTimer.increasing,
        },
      ],
    };
  } else {
    return {
      runningTimers: [],
    };
  }
};

export async function GET(request: NextRequest) {
  return executeIfAuthenticatedReturn401IfNot(
    request,
    async (_request, token) => {
      const user: User = await getUserData(token.uid);
      return NextResponse.json(mapResponse(user), { status: 200 });
    }
  );
}

export async function POST(request: NextRequest) {
  return executeIfAuthenticatedReturn401IfNot(
    request,
    async (_request, token) => {
      const newRunningTimer: RunningTimer = await request.json();
      if (!newRunningTimer.startDate) {
        return NextResponse.json(
          { error: "The start date must be set" },
          { status: 400 }
        );
      }
      if (newRunningTimer.increasing === undefined) {
        return NextResponse.json(
          { error: "The increasing field must be set" },
          { status: 400 }
        );
      }
      await updateExistingTimer(
        token.uid,
        newRunningTimer.startDate,
        newRunningTimer.increasing,
        newRunningTimer.description
      );
      return NextResponse.json({ status: 201 });
    }
  );
}

export async function DELETE(request: NextRequest) {
  return executeIfAuthenticatedReturn401IfNot(
    request,
    async (_request, token) => {
      await resetExistingTimer(token.uid);
      return NextResponse.json({ status: 204 });
    }
  );
}
