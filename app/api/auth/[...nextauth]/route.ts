import NextAuth, { NextAuthOptions } from "next-auth";
import GoogleProvider from "next-auth/providers/google";

if (
  process.env.GOOGLE_ID === undefined ||
  process.env.GOOGLE_SECRET === undefined
) {
  throw new Error(
    `Google Client ID and Google Client Secret have to be set. ClientId: ${process.env.GOOGLE_ID} ClientSecret: ${process.env.GOOGLE_SECRET}`
  );
}

export const authOptions: NextAuthOptions = {
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID,
      clientSecret: process.env.GOOGLE_SECRET,
    }),
  ],
  callbacks: {
    async session({ session, token }) {
      if (session?.user) {
        session.user.uid = token.uid;
      }
      return session;
    },
    async jwt({ token, account, user }) {
      if (user) {
        token.uid = `${account?.provider}${account === undefined ? "" : "-"}${
          user.id
        }`;
      }
      return token;
    },
  },
  session: {
    strategy: "jwt",
  },
};

const handler = NextAuth(authOptions);

export { handler as GET, handler as POST };
