import type { DefaultUser } from "next-auth";

declare module "next-auth/jwt" {
  interface JWT {
    uid: string;
  }
}

declare module "next-auth" {
  interface Session {
    user?: DefaultUser & {
      uid: string;
    };
  }
}
