import { JWT, getToken } from "next-auth/jwt";
import { NextRequest, NextResponse } from "next/server";

export const executeIfAuthenticatedReturn401IfNot = async (
  request: NextRequest,
  toExecute: (request: NextRequest, jwt: JWT) => Promise<NextResponse>
) => {
  const token = await getToken({ req: request });
  if (token) {
    return toExecute(request, token);
  } else {
    return NextResponse.json(null, { status: 401 });
  }
};
