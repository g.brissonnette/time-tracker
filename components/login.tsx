"use client";
import { signIn } from "next-auth/react";

export default function Login() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-center w-screen">
      <button onClick={() => signIn(undefined, { callbackUrl: "/" })}>
        Please login
      </button>
    </main>
  );
}
