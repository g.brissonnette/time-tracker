import { getServerSession } from "next-auth";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";

const UserInfo = async () => {
  const session = await getServerSession(authOptions);
  return (
    <div>
      <p>Hello {session?.user?.name}</p>
    </div>
  );
};

export default UserInfo;
