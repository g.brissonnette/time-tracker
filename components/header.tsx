import { getServerSession } from "next-auth";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import Account from "./account";

const Header = async () => {
  const session = await getServerSession(authOptions);
  return (
    <div className="flex flex-row-reverse space-x-1 w-screen p-4">
      {session ? <Account /> : null}
    </div>
  );
};

export default Header;
