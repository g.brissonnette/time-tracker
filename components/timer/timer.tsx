import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";

dayjs.extend(duration);

const Timer = ({ initialSeconds }: { initialSeconds: number }) => {
  return (
    <p className="font-mono font-medium text-5xl mb-3 lg:text-8xl">
      {dayjs.duration(initialSeconds, "seconds").format("HH:mm:ss")}
    </p>
  );
};

export default Timer;
