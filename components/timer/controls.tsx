import { useRef } from "react";

const Controls = ({
  timeIsRunning,
  isInIncrementalMode,
  onIncrease,
  onDecrease,
  onAdd,
  onRemove,
  onStop,
}: {
  timeIsRunning: boolean;
  isInIncrementalMode: boolean;
  onIncrease: (descriptionMessage: string | undefined) => void;
  onDecrease: (descriptionMessage: string | undefined) => void;
  onAdd: (
    amountOfMinutes: number,
    descriptionMessage: string | undefined
  ) => void;
  onRemove: (
    amountOfMinutes: number,
    descriptionMessage: string | undefined
  ) => void;
  onStop: () => void;
}) => {
  const inputDescriptionRef = useRef<HTMLInputElement>(null);
  const inputMinuteRef = useRef<HTMLInputElement>(null);

  const descriptionMessage = () => {
    return inputDescriptionRef.current?.value;
  };

  const amountOfMinutes = () => {
    const inputValue = inputMinuteRef.current?.value;
    const amoutOfMinutes = Number.parseInt(inputValue ?? "");
    return Number.isNaN(amoutOfMinutes) ? 0 : amoutOfMinutes;
  };

  const resetFields = () => {
    if (inputMinuteRef.current) {
      inputMinuteRef.current.value = "";
    }
    if (inputDescriptionRef.current) {
      inputDescriptionRef.current.value = "";
    }
  };

  const increase = () => {
    onIncrease(descriptionMessage());
    resetFields();
  };

  const decrease = () => {
    onDecrease(descriptionMessage());
    resetFields();
  };

  const add = () => {
    onAdd(amountOfMinutes(), descriptionMessage());
    resetFields();
  };

  const remove = () => {
    onRemove(-1 * amountOfMinutes(), descriptionMessage());
    resetFields();
  };

  return (
    <div className="flex flex-row justify-between w-full">
      {timeIsRunning ? (
        <div className="flex w-full justify-center">
          <button className="border py-1 px-2 min-w-[100px]" onClick={onStop}>
            stop
          </button>
        </div>
      ) : (
        <div className="flex flex-col w-full">
          <div className="flex flex-row mb-2 w-full xs:justify-center">
            <input
              className="text-black w-full"
              placeholder="What are you doing ?"
              ref={inputDescriptionRef}
            />
            {!isInIncrementalMode && (
              <input
                className="text-black w-12 ml-2"
                placeholder="min."
                maxLength={4}
                ref={inputMinuteRef}
                inputMode="numeric"
                type="number"
                max="9999"
              />
            )}
          </div>
          <div className="flex flex-row justify-between">
            {isInIncrementalMode ? (
              <>
                <button className="border py-1 px-2" onClick={increase}>
                  increase
                </button>
                <button className="border py-1 px-2" onClick={decrease}>
                  decrease
                </button>
              </>
            ) : (
              <>
                <button className="border py-1 px-2" onClick={add}>
                  add
                </button>
                <button className="border py-1 px-2" onClick={remove}>
                  remove
                </button>
              </>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default Controls;
