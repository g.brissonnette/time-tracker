"use client";
import { useEffect, useState } from "react";
import Controls from "./controls";
import Timer from "./timer";
import { HistoryData } from "@/types/history";
import useSWR from "swr";
import fetcher, { DELETERequest, POSTRequest } from "@/technical/api/fetchers";
import useSWRMutation from "swr/mutation";
import { useRouter } from "next/navigation";
import { TimerHistoryAPI } from "@/types/user";
import { RunningTimer, RunningTimersAPI } from "@/types/runningTimer";
import { RUNNING_TIMER_API_PATH, HISTORY_API_PATH } from "@/app/api/path";
import dayjs from "dayjs";

const computeCurrentBalance = (history: HistoryData[]) => {
  return history.reduce((prev, curr) => prev + curr.amountOfSeconds, 0) ?? 0;
};

const useCreateNewHistory = () => {
  return useSWRMutation(HISTORY_API_PATH, POSTRequest);
};

const useGetHistory = () => {
  return useSWR<TimerHistoryAPI>(HISTORY_API_PATH, fetcher);
};

const useCreateNewRunningTimer = () => {
  return useSWRMutation(RUNNING_TIMER_API_PATH, POSTRequest);
};

const useDeleteCurrentRunningTimer = () => {
  return useSWRMutation(RUNNING_TIMER_API_PATH, DELETERequest);
};

const useGetCurrentRunningTimers = () => {
  return useSWR<RunningTimersAPI>(RUNNING_TIMER_API_PATH, fetcher);
};

const ControlePaneReady = ({
  timerHistory,
  isInIncrementalMode,
  runningTimer,
  onIncrease,
  onDecrease,
  onStop,
}: {
  timerHistory: HistoryData[];
  isInIncrementalMode: boolean;
  runningTimer?: {
    startDate: Date;
    message?: string;
    increasing: boolean;
  };
  onIncrease?: () => void;
  onDecrease?: () => void;
  onStop?: () => void;
}) => {
  const [balanceInSeconds, setBalanceInSeconds] = useState(0);
  const [increasing, setIncreasing] = useState(false);
  const [decreasing, setDecreasing] = useState(false);
  const [timeIsRunning, setTimeIsRunning] = useState(false);
  const [elapsedSeconds, setElapsedSeconds] = useState(0);
  const [descriptionMessage, setDescriptionMessage] = useState<
    string | undefined
  >(undefined);
  const [localRunningTimer, setLocalRunningTimer] = useState(runningTimer);
  const { trigger: updateHistory } = useCreateNewHistory();
  const { trigger: postNewRunningTimer } = useCreateNewRunningTimer();
  const { trigger: deleteCurrentRunningTimer } = useDeleteCurrentRunningTimer();
  const router = useRouter();

  useEffect(() => {
    if (localRunningTimer) {
      const diff = dayjs().diff(localRunningTimer.startDate, "seconds");
      setDescriptionMessage(localRunningTimer.message);
      if (localRunningTimer.increasing) {
        setElapsedSeconds(diff);
        setIncreasing(true);
      } else {
        setElapsedSeconds(diff * -1);
        setDecreasing(true);
      }
      setLocalRunningTimer(undefined);
    }
  }, [localRunningTimer]);

  useEffect(() => {
    setBalanceInSeconds(computeCurrentBalance(timerHistory ?? []));
  }, [timerHistory]);

  useEffect(() => {
    setTimeIsRunning(increasing || decreasing);
  }, [increasing, decreasing]);

  useEffect(() => {
    let timer: NodeJS.Timeout;
    if (increasing) {
      timer = setInterval(
        () => setElapsedSeconds((elapsedSeconds) => elapsedSeconds + 1),
        1000
      );
    }
    return () => {
      clearInterval(timer);
    };
  }, [increasing]);

  useEffect(() => {
    let timer: NodeJS.Timeout;
    if (decreasing) {
      timer = setInterval(
        () => setElapsedSeconds((elapsedSeconds) => elapsedSeconds - 1),
        1000
      );
    }
    return () => {
      clearInterval(timer);
    };
  }, [decreasing]);

  const createNewRunningTimer = (
    increasing: boolean,
    descriptionMessage?: string
  ) => {
    const runningTimer: RunningTimer = {
      startDate: new Date(),
      description: descriptionMessage,
      increasing,
    };
    postNewRunningTimer(runningTimer);
  };

  const _onIncrease = (descriptionMessage?: string) => {
    onIncrease && onIncrease();
    setIncreasing(true);
    setDecreasing(false);
    setDescriptionMessage(descriptionMessage);
    createNewRunningTimer(true, descriptionMessage);
  };

  const _onDecrease = (descriptionMessage?: string) => {
    onDecrease && onDecrease();
    setDecreasing(true);
    setIncreasing(false);
    setDescriptionMessage(descriptionMessage);
    createNewRunningTimer(false, descriptionMessage);
  };

  const _onStop = async () => {
    onStop && onStop();
    setDecreasing(false);
    setIncreasing(false);
    await updateBalanceAndCreateNewHistory(elapsedSeconds, descriptionMessage);
  };

  const updateBalanceAndCreateNewHistory = async (
    amountOfSeconds: number,
    descriptionMessage?: string
  ) => {
    setBalanceInSeconds((prev) => prev + amountOfSeconds);
    const entry = {
      date: new Date(),
      description: descriptionMessage ?? "no description",
      amountOfSeconds,
      balanceInSeconds: balanceInSeconds + amountOfSeconds,
    };
    const toSave = timerHistory ?? [];
    toSave.unshift(entry);
    await updateHistory({ history: toSave });
    setElapsedSeconds(0);
    deleteCurrentRunningTimer();
    router.refresh();
  };

  const convertMinutesToSeconds = (amountOfMinutes: number) => {
    return amountOfMinutes * 60;
  };

  const updateBalanceWithMinutes = async (
    amountOfMinutes: number,
    descriptionMessage?: string
  ) => {
    const amountOfSeconds = convertMinutesToSeconds(amountOfMinutes);
    await updateBalanceAndCreateNewHistory(amountOfSeconds, descriptionMessage);
  };

  const onAdd = async (
    amountOfMinutes: number,
    descriptionMessage?: string
  ) => {
    await updateBalanceWithMinutes(amountOfMinutes, descriptionMessage);
  };

  const onRemove = async (
    amountOfMinutes: number,
    descriptionMessage?: string
  ) => {
    await updateBalanceWithMinutes(amountOfMinutes, descriptionMessage);
  };

  return (
    <div className="flex flex-col items-center">
      <Timer
        initialSeconds={
          timeIsRunning ? balanceInSeconds + elapsedSeconds : balanceInSeconds
        }
      />
      <Controls
        timeIsRunning={timeIsRunning}
        onIncrease={_onIncrease}
        onDecrease={_onDecrease}
        onStop={_onStop}
        isInIncrementalMode={isInIncrementalMode}
        onAdd={onAdd}
        onRemove={onRemove}
      />
    </div>
  );
};

const ControlePaneLoadingRunningTimer = ({
  timerHistory,
  isInIncrementalMode,
  onIncrease,
  onDecrease,
  onStop,
}: {
  timerHistory: HistoryData[];
  isInIncrementalMode: boolean;
  onIncrease?: () => void;
  onDecrease?: () => void;
  onStop?: () => void;
}) => {
  const { data, error, isLoading } = useGetCurrentRunningTimers();

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error</div>;
  }

  const currentRunningTimer = data?.runningTimers[0];
  if (currentRunningTimer?.startDate) {
    return (
      <ControlePaneReady
        timerHistory={timerHistory}
        runningTimer={{
          startDate: currentRunningTimer.startDate,
          message: currentRunningTimer.description,
          increasing: currentRunningTimer.increasing,
        }}
        onDecrease={onDecrease}
        onIncrease={onIncrease}
        onStop={onStop}
        isInIncrementalMode={isInIncrementalMode}
      />
    );
  } else {
    return (
      <ControlePaneReady
        timerHistory={timerHistory}
        onDecrease={onDecrease}
        onIncrease={onIncrease}
        onStop={onStop}
        isInIncrementalMode={isInIncrementalMode}
      />
    );
  }
};

const ControlePane = ({
  onIncrease,
  onDecrease,
  onStop,
}: {
  onIncrease?: () => void;
  onDecrease?: () => void;
  onStop?: () => void;
}) => {
  const { data, error, isLoading } = useGetHistory();
  const [isIncremental, setIsIncremental] = useState<boolean>(true);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error</div>;
  }
  return (
    <>
      <ControlePaneLoadingRunningTimer
        timerHistory={data?.history ?? []}
        isInIncrementalMode={isIncremental}
        onDecrease={onDecrease}
        onIncrease={onIncrease}
        onStop={onStop}
      />
      <div className="flex items-center justify-center space-x-4 my-2">
        <button
          className="text-xs text-slate-400  border border-slate-400 font-mono font-semibold p-0.5 mt-2"
          onClick={() => setIsIncremental(true)}
        >
          inc
        </button>
        <button
          className="text-xs text-slate-400 border border-slate-400 font-mono font-semibold p-0.5 mt-2"
          onClick={() => setIsIncremental(false)}
        >
          abs
        </button>
      </div>
    </>
  );
};

export default ControlePane;
