import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import { kv } from "@vercel/kv";
import { getServerSession, Session } from "next-auth";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import { User } from "@/types/user";

dayjs.extend(duration);

const getHistoricalData = async (userId: string | undefined) => {
  if (userId) {
    const data: User | null = await kv.get(userId);
    return data?.history ?? [];
  } else {
    return [];
  }
};

const formatDuration = (numberOfSeconds: number): string => {
  const isNegative = numberOfSeconds < 0;
  const durationWithAbsoluteNumberOfSeconds = dayjs
    .duration(Math.abs(numberOfSeconds), "seconds")
    .format("HH:mm:ss");
  return `${isNegative ? "-" : "+"} ${durationWithAbsoluteNumberOfSeconds}`;
};

const TimerHistory = async () => {
  const session = await getServerSession(authOptions);

  return (
    <table className="table-auto w-full text-sm lg:text-base">
      <thead>
        <tr className="text-left">
          <th>Date</th>
          <th className="hidden lg:block">Description</th>
          <th>Seconds</th>
          <th>Balance</th>
        </tr>
      </thead>
      <tbody>
        {(await getHistoricalData(session?.user?.uid)).map((d, i) => (
          <tr key={i}>
            <td>{dayjs(d.date).format("DD-MM-YYYY HH:mm:ss")}</td>
            <td className="hidden lg:block">{d.description}</td>
            <td>{formatDuration(d.amountOfSeconds)}</td>
            <td>
              {dayjs.duration(d.balanceInSeconds, "seconds").format("HH:mm:ss")}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default TimerHistory;
