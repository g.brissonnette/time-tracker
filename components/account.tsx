"use client";
import { signOut } from "next-auth/react";

const Account = () => {
  return <button onClick={() => signOut({ callbackUrl: "/" })}>Logout</button>;
};

export default Account;
