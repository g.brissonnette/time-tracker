import { HistoryData } from "./history";
import { RunningTimer } from "./runningTimer";

export type TimerHistoryAPI = {
  history: HistoryData[];
};

export type User = TimerHistoryAPI & {
  currentRunningTimer: RunningTimer;
};
