export type RunningTimer = {
  startDate?: Date;
  description?: string;
  increasing: boolean;
};

export type RunningTimersAPI = {
  runningTimers: RunningTimer[];
};
