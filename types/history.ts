export type HistoryData = {
  date: Date;
  description: string;
  amountOfSeconds: number;
  balanceInSeconds: number;
};
