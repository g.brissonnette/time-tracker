import { User } from "@/types/user";
import { kv } from "@vercel/kv";

const emptyObject: User = {
  currentRunningTimer: {
    startDate: undefined,
    increasing: false,
    description: undefined,
  },
  history: [],
};

export const getUserData = async (userId: string): Promise<User> => {
  return (await kv.get(userId)) ?? emptyObject;
};
