import { TimerHistoryAPI, User } from "@/types/user";
import { getUserData } from "./user";
import { kv } from "@vercel/kv";

export const replaceExistingUserHistory = async (
  userId: string,
  newHistory: TimerHistoryAPI
) => {
  const user: User = await getUserData(userId);
  const data: User = {
    ...user,
    history: newHistory.history,
  };
  await kv.set(userId, data);
  return true;
};
