import { User } from "@/types/user";
import { getUserData } from "./user";
import { kv } from "@vercel/kv";

const updateUserTimer = async (
  userId: string,
  increasing: boolean,
  startDate?: Date,
  description?: string
) => {
  const user: User = await getUserData(userId);
  const data: User = {
    ...user,
    currentRunningTimer: {
      startDate,
      description,
      increasing,
    },
  };
  await kv.set(userId, data);
  return true;
};

export const updateExistingTimer = async (
  userId: string,
  startDate: Date,
  increasing: boolean,
  description?: string
) => {
  await updateUserTimer(userId, increasing, startDate, description);
  return true;
};

export const resetExistingTimer = async (userId: string) => {
  await updateUserTimer(userId, false, undefined, undefined);
  return true;
};
